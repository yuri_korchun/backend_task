﻿module RateLinesParser

open Return
open TryParser

type currency =
    {
        CurrencyCode : string
    }

type exchangeRatePlain =
    {
        CountryName: string
        CurrencyName: string
        RateScale: string
        RateCurrencyCode : string 
        RateValue : string
    }


type exchangeRate =
    {
        SourceCurrency : currency
        TargetCurrency : currency 
        Value : decimal 
    }

let SplitLine (line : string) = 
    Seq.toArray (line.Split [|'|'|]) 

let Currencies currencyCodes =  
    currencyCodes 
       |> List.map ( fun currencyCode -> { CurrencyCode = currencyCode };  )

let ParseExchangeRatePlain (words : string[]) currencyCodes  = 
    let currencies = Currencies currencyCodes    
    if words.Length <> 5 then 
        ReturnError returnType.ConversionError "Stamp"
    elif words.[0] = "Country" then 
        ReturnError returnType.ConversionError "Header"
    elif not (List.exists (fun elem -> elem.CurrencyCode = words.[3]) currencies) then 
        ReturnError returnType.ConversionError "Don't process"
    else 
        { 
            CountryName = words.[0];
            CurrencyName = words.[1];
            RateScale = words.[2];
            RateCurrencyCode = words.[3];
            RateValue = words.[4]; 
        } |> ReturnOk 

let GenerateExchangeRatePlains ( lines: seq<string> ) currencies  = 
    lines 
    |> Seq.map ( fun line -> ParseExchangeRatePlain  (SplitLine line) currencies)
    |> Seq.filter (fun result -> IsSuccess result)
    |> Seq.map ( fun result -> result.ReturnValue.Value)
    

let CreateExchangeRate ( ratePlain: exchangeRatePlain) (baseCurrencyCode:string) =
    let rateValueResult = TryParseDecimal ratePlain.RateValue
    if IsSuccess rateValueResult then
        let rateScaleResult = TryParseDecimal ratePlain.RateScale
        if (IsSuccess rateScaleResult && rateScaleResult.ReturnValue.Value <> (decimal) 0.) then 
            { 
                SourceCurrency = { CurrencyCode = ratePlain.RateCurrencyCode};
                TargetCurrency = { CurrencyCode = baseCurrencyCode};
                Value = (decimal) ratePlain.RateValue / (decimal) ratePlain.RateScale; 
            } |> ReturnOk 
        else 
            ReturnError rateScaleResult.ReturnType rateScaleResult.ReturnMessage
    else 
        ReturnError rateValueResult.ReturnType rateValueResult.ReturnMessage

    
    
let GenerateExhangeRates ( ratePlains: seq<exchangeRatePlain> ) (baseCurrencyCode:string) = 
    ratePlains 
    |> Seq.map ( fun ratePlain -> CreateExchangeRate  ratePlain baseCurrencyCode )




