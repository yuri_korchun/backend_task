﻿namespace ExchangeRateUpdater
{
    public class ExchangeRateSource
    {
        public ExchangeRateSource(
            string countryName,
            string currencyName,
            string rateScale,
            string currencyCode,
            string rateValue)
        {
            CountryName = countryName;
            CurrencyName = currencyName;
            RateScale = rateScale;
            CurrencyCode = currencyCode;
            RateValue = rateValue;
        }

        public Result Result { get; }
        public string CountryName { get; }
        public string CurrencyName { get; }
        public string RateScale { get; }
        public string CurrencyCode { get; }
        public string RateValue { get; }

        public override string ToString()
        {
            return $"{CurrencyCode}={RateValue}";
        }
    }
}
